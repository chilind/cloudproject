## Broker settings.
import os
floating_ip = '130.238.29.100'
broker_user = 'proj'
broker_pass = 'losenord'

BROKER_URL = 'amqp://proj:losenord@130.238.29.100:5672/projVH'

# List of modules to import when celery starts.
CELERY_IMPORTS = ('tasks', )

CELERY_RESULT_BACKEND = 'amqp://'

CELERY_ACKS_LATE = True
CELERYD_PREFETCH_MULTIPLIER = 1
