import json
import time
from tasks import runAll, parse
from createInstance import createWorker
from killInstance import kill
from celery import Celery, subtask, group
from flask import Flask, render_template, request, jsonify
from novaclient.client import Client

app = Flask(__name__)

@app.route('/')
def index():
        return render_template('home.html')

@app.route('/run', methods=['POST'])
def run():
        filenames = []
        mfiles = []
        #Get user imput
        angle_start = request.form['angle_start']
        angle_stop = request.form['angle_stop']
        n_angles = request.form['n_angles']
        n_nodes = request.form['n_nodes']
        n_levels = request.form['n_levels']
        num_samples = request.form['num_samples']
        viscosity = request.form['viscosity']
        time1 = request.form['time']
        speed = request.form['speed']
        increment = (int(angle_stop)-int(angle_start))/int(n_angles)
        angles = range(int(angle_start), int(angle_stop), increment)
        for level in range(int(n_levels)):
                for angle in angles:
                        filenames.append("r"+str(level)+"a"+str(angle)+"n"+n_nodes+".msh")

        #Set up novaclient to enable the broker to create workers.
        #Hard coding this is not good but I found no way to create an instance without hard coding.
        config = {'username':'chli9685',
                  'api_key':'cRUtu=a7',
                  'project_id':'ACC-Course',
                  'auth_url':'http://smog.uppmax.uu.se:5000/v2.0',
                   }
        nc = Client('2',**config)

        #Check how many workers there are already
        serverlist = nc.servers.findall()
        runningServers = 0
        for server in serverlist:
            if(server.name.startswith("CprojWorker")):
                runningServers += 1

        #Create worker if necessary
        if (runningServers * 4) < int(n_angles):
            i = (int(n_angles) - runningServers*4)/4
            if i == 0:
                i = 1
            createWorker(nc,i,runningServers)


        #Send jobs to workers
        #startTime = time.time()
        job_list = [runAll.s(str(index),n_nodes,n_levels,filenames[angles.index(index)],num_samples,viscosity,speed,time1) for index in angles]
        jobs = group(job_list)
        run = jobs.apply_async()
        resultParse = run.get()
        #print 'Time taken = {}'.format((time.time() - startTime))

        #Save results
        #for index in angles:
        #    filename=filenames[angles.index(index)]
        for i in (resultParse):
            data = {}
            data['id'] = i['filename']
            data['data'] = i['data']
            mfiles.append(data)

        #Terminate all workers (wanted feature?):
        #print "Terminating all workers"
        #kill(nc,0,1)

        #Send results to flask to be displayed
        json_obj = json.dumps(mfiles)
        return render_template('run.html', json = json_obj)


if __name__ == "__main__":
        app.run(host='0.0.0.0',debug= True)
