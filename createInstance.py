#Howto:
#1 source ACC-Course-openrc.sh
#2 run 'python createInstance 1 2 0' to create 1 broker and 2 workers with numbers 0, 1
import os, time, sys

def createBroker(nc):
    broker = int(sys.argv[1])
    workers = int(sys.argv[2])

    keypair = nc.keypairs.find(name="chliKey")

    nc.images.list()
    image = nc.images.find(name="CprojBrokerSnap")
    flavor = nc.flavors.find(name="m1.medium")

    nc.networks.list()
    network = nc.networks.find(label="ACC-Course-net")

    print "Getting userdata..."
    userData = open("udBroker.yml","r")

    print "Creating server..."
    server = nc.servers.create(name = "CprojBroker", image = image, flavor = flavor, network = network, key_name = keypair.name, userdata=userData)

    status = server.status
    while status == 'BUILD':
        time.sleep(5)
        # Retrieve the instance again so the status field updates
        instance = nc.servers.get(server.id)
        status = instance.status
    print "status: %s" % status

    floating_ip = '130.238.29.100'
    print "Attaching IP:"
    print floating_ip
    server.add_floating_ip(floating_ip)


def createWorker(nc, workers, beginNr):
    workerIPs = []
    keypair = nc.keypairs.find(name="chliKey")

    for x in range(beginNr,(workers+beginNr)):
        nc.images.list()
        image = nc.images.find(name="CprojWorkerSnap")
        flavor = nc.flavors.find(name="m1.medium")

        nc.networks.list()
        network = nc.networks.find(label="ACC-Course-net")

        print "Getting userdata..."
        userData = open("/home/ubuntu/cloudproject/udWorker.yml","r")

        print "Creating server..."
        server = nc.servers.create(name = "CprojWorker{}".format(x), image = image, flavor = flavor, network = network, key_name = keypair.name, userdata=userData)

        status = server.status
        while status == 'BUILD':
            time.sleep(5)
            # Retrieve the instance again so the status field updates
            instance = nc.servers.get(server.id)
            status = instance.status
        print "status: %s" % status

        floating_ip = ''
        while floating_ip == '':
            iplist = nc.floating_ips.list()
            for ip_obj in iplist:
                if ((getattr(ip_obj,'instance_id')) == None):
                    floating_ip = getattr(ip_obj, 'ip')
                    workerIPs.append(floating_ip)
                    break

        print "Attaching IP:"
        print floating_ip
        server.add_floating_ip(floating_ip)


def init():
    config = {'username':os.environ['OS_USERNAME'],
              'api_key':os.environ['OS_PASSWORD'],
              'project_id':os.environ['OS_TENANT_NAME'],
              'auth_url':os.environ['OS_AUTH_URL'],
               }
    from novaclient.client import Client
    return Client('2',**config)


def run():
    try:
        nc = init()
    except:
        print "You need to source first!"
        sys.exit(0)

    if int(sys.argv[1]) == 1:
        createBroker(nc)
    if int(sys.argv[2]) >= 1:
        if len(sys.argv) >= 4:
            createWorker(nc, int(sys.argv[2]), int(sys.argv[3]))
        else:
            createWorker(nc, int(sys.argv[2]), 0)

if __name__ == '__main__':
    run()
