import subprocess
from celery import Celery


#app = Celery('tasks', backend='amqp', broker='amqp://proj:losenord@130.238.29.100:5672/projVH')
app = Celery('tasks')
app.config_from_object('celeryconfig')

#Generate mesh files
def genMSH(angle_start, angle_stop, n_angles, n_nodes, n_levels):
	#print "Generating MSH-file with {}{}{}{}{}".format(angle_start,angle_stop,n_angles,n_nodes,n_levels)
	try:
		subprocess.check_call('sudo /home/ubuntu/cloudproject/naca_airfoil/run.sh ' + angle_start + ' ' + angle_stop + ' ' + n_angles + ' ' + n_nodes + ' ' + n_levels, shell=True)
		subprocess.check_call('sudo chown -R ubuntu /home/ubuntu/cloudproject/naca_airfoil/msh', shell=True)
		subprocess.check_call('sudo chown -R ubuntu /home/ubuntu/cloudproject/naca_airfoil/geo', shell=True)
	except Exception as e:
		print "Error generating MSH-file!"
		print e

#Convert mesh files to xml so airfoil can read
def convert(filename):
	try:
		command = 'sudo chmod 777 /home/ubuntu/project/msh/'+filename
		subprocess.call(command, shell=True)
		name = "sudo dolfin-convert " + "./naca_airfoil/msh/" + filename + " ./naca_airfoil/msh/" + filename + ".xml"
		subprocess.check_call(name, shell=True)
	except:
		print "Error converting files!"

#Run airfoil
def airfoil(filename, num_samples, viscosity, speed, time):
	try:
		command = 'sudo chmod 777 /home/ubuntu/project/msh/'+filename
		subprocess.call(command, shell=True)
		name = 'sudo ./naca_airfoil/navier_stokes_solver/airfoil ' + num_samples + ' ' + viscosity + ' ' + speed + ' ' + time + ' naca_airfoil/msh/' + filename + ".xml"
		subprocess.check_call(name, shell=True)
	except:
		print "Error running airfoil!"

#Read airfoil results
def parse():
	f = open('/home/ubuntu/cloudproject/results/drag_ligt.m')
	lines = []
	dataArray = []
	for line in f:
		lines.append(line)
	lines.pop(0)
	for line in lines:
		data={}
		split = line.split("\t")
		try:
			data['time'] = split[0]
			data['lift'] = split[1]
			data['drag'] = split[2]
		except:
			pass
		dataArray.append(data)
	return dataArray


@app.task
def runAll(gen,n_nodes,n_levels,filename, num_samples, viscosity, speed, time):
	genMSH(gen,gen,'1',n_nodes,n_levels)
	convert(filename)
	airfoil(filename,num_samples,viscosity,speed,time)
	resultParse = {'data':parse(), 'filename':filename}
	return resultParse
