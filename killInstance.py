#Howto:
#1 run 'source ACC-Course-openrc.sh'
#2 run 'python killInstance 0 1' to kill worker but not broker
import os, sys

def kill(nc,broker,worker):
    serverlist = nc.servers.findall()

    if broker == 1:
        for server in serverlist:
            if(server.name == "CprojBroker"):
                print "Terminating broker: " + server.name
                server.delete()

    if worker == 1:
        for server in serverlist:
            if(server.name.startswith("CprojWorker")):
                print "Terminating worker: " + server.name
                server.delete()


def init():
    config = {'username':os.environ['OS_USERNAME'],
              'api_key':os.environ['OS_PASSWORD'],
              'project_id':os.environ['OS_TENANT_NAME'],
              'auth_url':os.environ['OS_AUTH_URL'],
               }
    from novaclient.client import Client
    return Client('2',**config)


def run():
    try:
        nc = init()
    except:
        print "You need to source first!"
        sys.exit(0)

    kill(nc,int(sys.argv[1]),int(sys.argv[2]))

if __name__ == '__main__':
    run()
